import os
import warnings
import json
import joblib as jb

import click
import pandas as pd
import numpy as np
import xgboost as xgb

from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split

import mlflow
from mlflow.models.signature import infer_signature
from dotenv import load_dotenv

from src.data.preprocessing import convert_data_for_train, destroy_imbalance
from src.data.feature_engineering import generate_features
from src.data.feature_selection import select_features

warnings.filterwarnings("ignore")
warnings.simplefilter(action='ignore', category=FutureWarning)

np.random.seed(59)


load_dotenv()
remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_server_uri)


def read_data() -> pd.DataFrame:
    path = 'data/raw/train.csv'
    df = pd.read_csv(path)
    return df


def preprocess_data(df: pd.DataFrame) -> pd.DataFrame:
    df = convert_data_for_train(df)
    df = generate_features(df)
    df = select_features(df)
    return df


def train_model(X, y):
    '''
        After tuning with hyperopt^
        Best model - XGBClassifier (among Catboost and LGBM)
        Best tuned and non-tuned parameters are below
    '''
    params = {
        'alpha': 0.003,
        'colsample_bytree': 0.8,
        'gamma': 5,
        'lambda': 0.003,
        'learning_rate': 0.007,
        'max_depth': 10,
        'min_child_weight': 0.96,
        'subsample': 0.53,
        'objective': 'binary:logistic',
        'eval_metric': 'auc',
        'seed': 59
    }

    train = xgb.DMatrix(X, y)
    model = xgb.train(params, train)

    return model


def save_model(model):
    model.save_model('models/model_xgb_new.json')


def load_model():
    model = xgb.XGBClassifier()
    model.load_model('models/model_xgb_new.json')
    return model


def train_old():
    # Read and preprocess the data
    df = read_data()
    df = preprocess_data(df)

    # Split features and target
    X, y = df.drop('churn', axis=1), df.churn

    # Deal with imbalance
    X, y = destroy_imbalance(X, y)

    # Fit the model
    model = train_model(X, y)

    # Save the model, so we can get it for the inference
    save_model(model)

    # Load the model (checking)
    model = load_model()
    preds = model.predict(X)
    print(classification_report(y, preds))


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def train_click(input_path: str, output_path: str):
    """
    Train the model and log params, metrics and artifacts in MLflow
    :param input_path: train dataframe
    :param output_path: model (for [0]) and score (for [1]) artifact's path
    :return: None
    """
    with mlflow.start_run():
        mlflow.get_artifact_uri()
        train_df = pd.read_csv(input_path)
        assert isinstance(train_df, pd.DataFrame), "input must be a valid dataframe"

        # Preprocess the data
        train_df = preprocess_data(train_df)

        # Split features and target
        X, y = train_df.drop('churn', axis=1), train_df.churn
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=59, stratify=y)

        # Deal with imbalance
        X_train, y_train = destroy_imbalance(X_train, y_train)

        # Create xgb datasets
        train = xgb.DMatrix(X_train, y_train)
        test = xgb.DMatrix(X_test, y_test)

        params = {
            'alpha': 0.003,
            'colsample_bytree': 0.8,
            'gamma': 5,
            'lambda': 0.003,
            'learning_rate': 0.007,
            'max_depth': 5,
            'min_child_weight': 0.96,
            'subsample': 0.53,
            'objective': 'binary:logistic',
            'eval_metric': 'auc',
            'seed': 59
        }

        model = xgb.train(params, train)

        os.makedirs(output_path, exist_ok=True)
        output_model_path = os.path.join(output_path, "model")
        jb.dump(model, output_model_path)

        y_pred_proba = model.predict(test)
        y_pred = np.where(y_pred_proba >= 0.5, 1, 0)
        class_report = classification_report(y_test, y_pred, output_dict=True)
        score = {
            "precision": class_report["1"]["precision"],
            "recall": class_report["1"]["recall"],
            "f1-score": class_report["1"]["f1-score"],
            "support": class_report["1"]["support"],
            "accuracy": class_report["accuracy"],
        }

        output_score_path = os.path.join(output_path, "score")
        with open(output_score_path, "w") as score_file:
            json.dump(score, score_file, indent=4)

        signature = infer_signature(X_test, y_pred)

        mlflow.log_params(params)
        mlflow.log_metrics(score)
        mlflow.xgboost.log_model(
            xgb_model=model,
            artifact_path="model",
            registered_model_name="churn_xgboost",
            signature=signature
        )

    experiment = dict(mlflow.get_experiment_by_name("Default"))
    experiment_id = experiment['experiment_id']
    df = mlflow.search_runs([experiment_id])
    best_run_id = df.loc[0, 'run_id']
    print(best_run_id)


if __name__ == "__main__":
    # data/raw/train.csv -> models/first_one
    train_click()
