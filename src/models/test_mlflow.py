import os

import mlflow
from mlflow.tracking import MlflowClient
from dotenv import load_dotenv


if __name__ == "__main__":
    load_dotenv()
    mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))
    client = MlflowClient()
    experiment = client.get_experiment(0)
    print(experiment)
