import warnings

import pandas as pd
import numpy as np

from src.models.train import load_model
from src.data.preprocessing import preprocess_data
from src.data.feature_engineering import generate_features
from src.data.feature_selection import select_features

warnings.filterwarnings("ignore")
warnings.simplefilter(action='ignore', category=FutureWarning)

np.random.seed(59)


def read_test_data() -> pd.DataFrame:
    path = 'data\\test.csv'
    df = pd.read_csv(path)
    return df


def make_save_prediction(model, data):
    ids = data['id'].astype(np.int32)
    X = data.drop('id', axis=1)

    preds = model.predict(X)
    submission_df = pd.DataFrame({
        'id': ids,
        'churn': preds
    })

    submission_df['churn'] = submission_df['churn'].map({1: 'yes', 0: 'no'})
    submission_df.to_csv('submissions\\first.csv', index=False)


if __name__ == "__main__":
    df = read_test_data()
    df = preprocess_data(df)
    df = generate_features(df)
    df = select_features(df)
    model = load_model()
    make_save_prediction(model, df)
