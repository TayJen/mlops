import click
import pandas as pd


def generate_features(df: pd.DataFrame) -> pd.DataFrame:
    """
        Generate features without saving

        :param df: DataFrame with preprocessed data
        :return: DataFrame with generated features
    """
    # Generate features of average month charge/minutes/calls.
    features = ['minutes', 'calls', 'charge']
    for feature in features:
        df['avg_mt_' + feature] = df['total_' + feature] / df['account_length']

    # Same for average call, how much does it cost, how long is it (in minutes)
    df['avg_call_charge'] = df['total_charge'] / df['total_calls']
    df['avg_intl_call_charge'] = df['total_intl_charge'] / df['total_intl_calls']

    df['avg_call_minutes'] = df['total_minutes'] / df['total_calls']
    df['avg_intl_call_minutes'] = df['total_intl_minutes'] / df['total_intl_calls']

    # New feature as intersection of international plan and voice_mail plan
    df['both_plans'] = df['international_plan'] & df['voice_mail_plan']

    # Fill NaNs
    df.fillna(0, inplace=True)

    return df


@click.command()
@click.argument("input_path", type=click.Path())
@click.argument("output_path", type=click.Path())
def generate_features_click(input_path: str, output_path: str):
    """
        Generate features for the preprocessed pandas dataframe

        :param input_path: Path to read preprocessed DataFrame
        :param output_path: Path to save DataFrame with generated features
        :return:
    """
    # Read the data
    df = pd.read_csv(input_path)

    # Generate features
    df = generate_features(df)

    # Save the data
    df.to_csv(output_path, index=False)


if __name__ == "__main__":
    # 'data/interim/test_preprocessed.csv' -> 'data/interim/test_many_features.csv'
    generate_features_click()
