import click
import pandas as pd


def select_features(df: pd.DataFrame) -> pd.DataFrame:
    # Drop strongly correlated features (threshold = 0.8)
    df.drop(['total_intl_charge', 'total_charge', 'avg_mt_minutes', 'avg_mt_calls',
             'avg_call_minutes', 'avg_intl_call_minutes', 'voice_mail_plan'], axis=1, inplace=True)

    # Drop `state` as this feature has low impact
    df.drop(['state_WV', 'state_NJ', 'state_MN',
             'state_MD', 'state_TX'], axis=1, inplace=True)

    return df


@click.command()
@click.argument("input_path", type=click.Path())
@click.argument("output_path", type=click.Path())
def select_features_click(input_path: str, output_path: str):
    """
        Select features for the preprocessed pandas dataframe with generated features

        :param input_path: Path to read preprocessed DataFrame
        :param output_path: Path to save DataFrame with selected features
        :return:
    """
    # Read the data
    df = pd.read_csv(input_path)

    # Select features
    df = select_features(df)

    # Save the data
    df.to_csv(output_path, index=False)


if __name__ == "__main__":
    # 'data/interim/test_many_features.csv' -> 'data/processed/data_for_inference.csv'
    select_features_click()
