from typing import Tuple

import click
import pandas as pd
from imblearn.combine import SMOTEENN


def drop_outliers(df: pd.DataFrame) -> pd.DataFrame:
    """
        Only for train set
    """
    # Remove outliers (less than 10 vmail messages and more than 50)
    mask_vm_true = (df.voice_mail_plan == 'yes')
    mask_vm_outlier = ((df.number_vmail_messages > 50) | (df.number_vmail_messages < 10))
    df = df.drop(df[mask_vm_true & mask_vm_outlier].index)

    # More or equal to 5 service calls for non-churn customers
    mask_ch_true = (df.churn == 'no')
    mask_ch_outlier = (df.number_customer_service_calls >= 5)
    df = df.drop(df[mask_ch_true & mask_ch_outlier].index)

    return df


def destroy_imbalance(X: pd.DataFrame, y: pd.Series) -> Tuple[pd.DataFrame, pd.Series]:
    """
        Only for train set
    """
    oversample = SMOTEENN()
    X, y = oversample.fit_resample(X, y)
    return X, y


def convert_data_for_train(df: pd.DataFrame) -> pd.DataFrame:
    """
        Only for train set
    """
    df = drop_outliers(df)
    df = preprocess_data(df, True)

    return df


def preprocess_data(df: pd.DataFrame, for_train: bool = False) -> pd.DataFrame:
    """
        Preprocess the data without saving

        :param df: DataFrame with raw data
        :param for_train: Whether to preprocess for train or for inference
        :return: DataFrame with preprocessed data
    """
    # Map categorical columns to numbers
    bin_columns = ['international_plan', 'voice_mail_plan']
    if for_train:
        bin_columns.append('churn')

    for col in bin_columns:
        df[col] = df[col].map({'yes': 1, 'no': 0})

    # Sum up total columns, instead of different times of a day
    feature_types = ['minutes', 'calls', 'charge']

    for feature in feature_types:
        df['total_' + feature] = df['total_day_' + feature] + df['total_eve_' + feature] + df['total_night_' + feature]
        df.drop(['total_day_' + feature, 'total_eve_' + feature, 'total_night_' + feature], axis=1, inplace=True)

    top5_states = ['WV', 'NJ', 'MN', 'MD', 'TX']
    df['state'] = df['state'].apply(lambda x: x if x in top5_states else 'other')
    df = pd.get_dummies(df, columns=['area_code', 'state'])

    return df


@click.command()
@click.argument("input_path", type=click.Path())
@click.argument("output_path", type=click.Path())
def preprocess_data_click(input_path: str, output_path: str, for_train: bool = False):
    """
        Preprocessing for raw pandas dataframe

        :param input_path: Path to read raw DataFrame
        :param output_path: Path to save preprocessed DataFrame
        :return:
    """
    # Read the data
    df = pd.read_csv(input_path)

    # Preprocess the data
    df = preprocess_data(df, for_train=for_train)

    # Save the data
    df.to_csv(output_path, index=False)


if __name__ == "__main__":
    # 'data/raw/test.csv' -> 'data/interim/test_preprocessed.csv'
    preprocess_data_click()
